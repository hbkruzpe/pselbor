CREATE DATABASE test_selbor CHARACTER SET utf8 COLLATE utf8_general_ci;
use test_selbor;
CREATE TABLE customer (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    address TEXT NULL DEFAULT NULL,
    country VARCHAR (255),
    phone VARCHAR (50),
    order_date TIMESTAMP NULL DEFAULT NULL,
    seller VARCHAR (255) NULL DEFAULT NULL,
    region VARCHAR (255),
    PRIMARY KEY (id)
);

CREATE TABLE invoice (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    customer_id INT UNSIGNED NOT NULL,
    order_date TIMESTAMP NULL DEFAULT NULL,
    subtotal DOUBLE(10, 2) NOT NULL,
    total DOUBLE(10, 2) NOT NULL,
    discount DOUBLE(10, 2) NOT NULL,
    region VARCHAR (255),
    seller VARCHAR (255) NULL DEFAULT NULL,
    PRIMARY KEY (id),
    INDEX customer_inovoice_id_foreign (customer_id),
    CONSTRAINT customer_inovoice_id_foreign FOREIGN KEY (customer_id) REFERENCES customer(id)
    );


CREATE USER 'hbselbor'@'localhost' identified by 'S3LB0R2021';
GRANT ALL PRIVILEGES ON test_selbor.* TO hbselbor@localhost;
FLUSH PRIVILEGES ;
exit;
