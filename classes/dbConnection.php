<?php

class DbConnection {

    private  $host;
    private  $user;
    private  $passwd;
    private  $database;

    private $connection = null;
    private $stmt =  null;
    private $result;

    function __construct() {
        $this->host = MYSQL_HOST;
        $this->database =  MYSQL_DATABASE;
        $this->user = MYSQL_USER;
        $this->passwd =  MYSQL_PASSWORD;
    }

    public function connectionToDB () {
        $this->connection =  mysqli_connect($this->host, $this->user, $this->passwd, $this->database)
             or die ("error" . mysqli_error($this->connection));
    }
    public function PrepareStmt($query, $params = []) {
        $this->connectionToDB();
        $this->stmt = mysqli_prepare($this->connection, $query);
        $parameters = [];
        $str = "";
        foreach ($params as $param) {
            $str .=$param['type'];
            array_push($parameters, $param['value']);
        }
        if(count($parameters))
            mysqli_stmt_bind_param($this->stmt, $str, ...$parameters);
    }
    public function ExecuteStmt()
    {
        mysqli_stmt_execute($this->stmt);
        $this->result = mysqli_stmt_get_result($this->stmt);

    }
    function GetStmtResult()
    {
        $retArray = [];
        $this->ExecuteStmt();
        while ($rs = mysqli_fetch_assoc($this->result)) {
            $retArray[] = $rs;
        }
        $this->CleanQuery();
        return $retArray;
    }
    function InsertStmt()
    {
        $this->ExecuteStmt();
        $last_id = mysqli_insert_id($this->connection);
        $this->CleanQuery();
        return $last_id;
    }

    public function CleanQuery() {
        mysqli_free_result($this->result);
    }

}