<?php
$db = new DbConnection();
$db->PrepareStmt('select * from customer');
$customers = $db->GetStmtResult();
?>
<html>
   <title>Prueba Selbor</title>
   <link rel="stylesheet" type="text/css" href='../css/style.css' />
   <body>
   <h3>Formulario de alta de clientes</h3>
   <div style="display: flex; justify-content: center">
       <form id="frmCustomer" name="frmCustomer" method="post" action="../module/customer.php">
           <div style="display: table">
              <div style="display: table-row">
                  <div style="display:table-cell; padding: 2px">
                      <div style="width: 100%"> * Nombre</div>
                      <div style="width: 100%"><input type="text" name="name" id="name"></div>
                  </div>
                  <div style="display:table-cell; padding: 2px">
                      <div style="width: 100%">* Telefono</div>
                      <div style="width: 100%"><input type="text" name="phone" id="phone"></div>
                  </div>
                  <div style="display:table-cell; padding: 2px">
                      <div style="width: 100%">* Direccion</div>
                      <div style="width: 100%"><input type="text" name="address" id="address"></div>
                  </div>
              </div>
               <div style="display: table-row">
                   <div style="display:table-cell; padding: 2px">
                       <div style="width: 100%">* Pais</div>
                       <div style="width: 100%"><input type="text" name="country" id="country"></div>
                   </div>
                   <div style="display:table-cell; padding: 2px">
                       <div style="width: 100%">* Fecha de pedido</div>
                       <div style="width: 100%"><input type="text" name="order_date" id="order_date"  placeholder="0000-00-00"></div>
                   </div>
                   <div style="display:table-cell; padding: 2px">
                       <div style="width: 100%">Vendedor</div>
                       <div style="width: 100%"><input type="text" name="seller" id="seller"></div>
                   </div>
               </div>
               <div style="display: table-row">
                   <div style="display:table-cell; padding: 2px">
                       <div style="width: 100%">* Region</div>
                       <div style="width: 100%"><input type="text" name="region" id="region"></div>
                   </div>
               </div>
           </div>
           <div>
               <p>*  Campos requeridos</p>
           </div>
           <div>
               <input  type="submit" class="send_button" value="Guardar" />
           </div>
       </form>
   </div>
   <h3>Lista de clientes</h3>
   <div class="container-table">
       <table>
           <thead>
           <tr>
               <th style="width: 5%">#</th>
               <th style="width: 20%">Nombre</th>
               <th style="width: 10%">Telefono</th>
               <th style="width: 10%">Direccion</th>
               <th style="width: 10%">Pais</th>
               <th style="width: 10%">Fecha de pedido</th>
               <th style="width: 10%">Vendedor</th>
               <th style="width: 10%">Region</th>
           </tr>
           </thead>
           <tbody>
           <?php foreach($customers as $key => $customer) { ?>
               <tr>
                   <td><?php echo $key + 1 ?></td>
                   <td style=" width:1px;white-space: nowrap"><?php echo $customer['name'] ?></td>
                   <td><?php echo $customer['phone'] ?></td>
                   <td><?php echo $customer['address'] ?></td>
                   <td><?php echo $customer['country'] ?></td>
                   <td><?php echo $customer['order_date'] ?></td>
                   <td><?php echo $customer['seller'] ?></td>
                    <td><?php echo $customer['region'] ?></td>
               </tr>
           <?php } ?>
           </tbody>
       </table>
   </div>
   <script type="text/javascript" src="../js/main.js"></script>
   </body>
</html>

