<?php
include_once('../init.php');
include_once('../config.php');
include_once('../libraries.php');
if($_POST) {
    $db = new DbConnection();
    $params = [];
    array_push($params, ['type'=> 's', 'value' => $_POST['name']]);
    array_push($params, ['type'=> 's', 'value' => $_POST['address']]);
    array_push($params, ['type'=> 's', 'value' => $_POST['country']]);
    array_push($params, ['type'=> 's', 'value' => $_POST['phone']]);
    array_push($params, ['type'=> 's', 'value' => $_POST['order_date']]);
    array_push($params, ['type'=> 's', 'value' => $_POST['seller']]);
    array_push($params, ['type'=> 's', 'value' => $_POST['region']]);
    $db->PrepareStmt('insert into customer(name, address, country, phone, order_date, seller, region)
                            values (?, ?, ?, ?, ?, ?, ?)', $params);
    $db->InsertStmt();
}