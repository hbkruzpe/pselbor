document.addEventListener("DOMContentLoaded", function(event) {

    function validateForm () {
        if(document.getElementById('name').value === '') {
            alert ('El campo nombre es requerido'); return false;
        }
        if(document.getElementById('phone').value === '') {
            alert ('El campo telefono es requerido'); return false;
        }
        if(document.getElementById('address').value === '') {
            alert ('El campo direccion es requerido'); return false;
        }
        if(document.getElementById('country').value === '') {
            alert ('El campo pais es requerido'); return false;
        }
        if(document.getElementById('order_date').value === '') {
            alert ('El campo Fecha de pedido es requerido'); return false;
        }
        if(document.getElementById('region').value === '') {
            alert ('El campo region es requerido'); return false;
        }
        return true
    }

    function sendForm() {
        if(!validateForm())
            return false

        const req = new XMLHttpRequest()
        const formData = new FormData( document.getElementById('frmCustomer'))
        req.addEventListener( "load", function(event) {
            document.getElementById('frmCustomer').reset()
            window.location.reload()
        } )

        req.addEventListener( "error", function( event ) {
            alert( 'Error al guardar formulario' )
        } );

        req.open( "POST", 'http://' + document.location.hostname +'/ajax/customer.php' );
        req.send( formData )
    }

    var form = document.getElementById('frmCustomer');
    form.addEventListener('submit', function (event){
        event.preventDefault()
        sendForm()
    })
});


